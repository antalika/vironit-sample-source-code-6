import abc
import asyncio
import random
import struct

from collections import OrderedDict

import bson
import requests

from meta_server.log import server_logger


HEADER_BUFFER_SIZE = 4  # in bytes

TYPE_CODES = {
    'ChatLoginPlayerRequest': 'lgrq',
    'ChatAddMemberToRoomRequest': 'amrq',
    'ChatBroadcastRoomMessageRequest': 'brmq'
}


class ChatError(ConnectionError):
    pass


class ChatConnectionTimeout(ChatError):
    pass


class ChatRequest:
    """Common class for all request types.
    If you need to create new request type you should inherit this one.
    """

    message_seq = 1

    def __init__(self):
        self.raw = self._pack()
        self.__class__.message_seq += 1

    def _pack(self):
        """The method packs request to packet"""
        data = struct.pack('=i', self.type_code_to_int(self.message_type_code))
        data += struct.pack('=i', self.message_seq)
        data += bson.dumps(self.get_body())
        header = struct.pack('=i', (len(data)))
        return header + data

    @abc.abstractmethod
    def get_body(self):
        """This method should be implemented in child class."""
        return

    @staticmethod
    def type_code_to_int(type_code):
        """
        The method converts string type_code to integer.
        :param type_code: type_code in string
        :return: type_code in integer
        """
        byte_array = [ord(c) for c in type_code]
        value = 0
        for byte in byte_array:
            value = value << 8 | (byte & 0xff)
        return value

    @property
    def message_type_code(self):
        return TYPE_CODES.get(self.__class__.__name__)

    @property
    def passthrough(self):
        rnd_hex = lambda n: ''.join((random.choice('1234567890abcdef') for i in range(n)))
        return '-'.join((rnd_hex(n) for n in (8, 4, 4, 4, 12)))

    def __str__(self):
        return '{0}: ({1})'.format(self.__class__.__name__, self.get_body())


class ChatLoginPlayerRequest(ChatRequest):
    def __init__(self, game_id, player_id, nickname):
        self.game_id = game_id
        self.player_id = player_id
        self.nickname = nickname
        super(self.__class__, self).__init__()

    def get_body(self):
        return OrderedDict((
            ('gameId', self.game_id),
            ('playerId', self.player_id),
            ('nickName', self.nickname),
            ('passthrough', self.passthrough)
        ))


class ChatAddMemberToRoomRequest(ChatRequest):
    def __init__(self, game_id, player_id, room_name, room_type):
        self.game_id = game_id
        self.player_id = player_id
        self.room_name = room_name
        self.room_type = room_type
        super(self.__class__, self).__init__()

    def get_body(self):
        return OrderedDict((
            ('playerId', self.player_id),
            ('target', self.room_name),
            ('type', self.room_type),
            ('gameId', self.game_id),
            ('passthrough', self.passthrough)
        ))


class ChatBroadcastRoomMessageRequest(ChatRequest):
    def __init__(self, game_id, player_id, room_names, message):
        self.game_id = game_id
        self.player_id = player_id
        self.room_names = room_names
        self.message = message
        super(self.__class__, self).__init__()

    def get_body(self):
        return OrderedDict((
            ('fromPlayerId', self.player_id),
            ('rooms', self.room_names),
            ('message', self.message),
            ('gameId', self.game_id),
            ('groupBy', ''),
            ('extraInfo', ''),
            ('passthrough', self.passthrough)
        ))


class ChatResponse:
    """Common class for all responses."""

    def __init__(self, raw_response):
        self.raw = raw_response
        self.header = struct.unpack('=i', raw_response[:4])[0]
        self.message_type_code = self.type_code_to_str(struct.unpack('=i', raw_response[4:8])[0])
        self.message_seq = struct.unpack('=i', raw_response[8:12])[0]
        self.body = bson.loads(raw_response[12:])

    @staticmethod
    def type_code_to_str(type_code):
        """
        The method converts integer type_code to string.
        :param type_code: type_code in integer
        :return: type_code in string
        """
        result = list()
        for i in range(3, -1, -1):
            bt = type_code >> (24 - i * 8) & 0xFF
            result.append(bt)
        return ''.join([chr(c) for c in result])

    def get_body(self):
        """Return body of response."""
        return self.body

    def is_error(self):
        response_status = self.body.get('responseStatus')
        if response_status:
            if response_status.get('errorMessage'):
                return True
            if response_status.get('statusCode') != 2000:
                return True
        return False

    def __str__(self):
        return '{0}: ({1})'.format(self.__class__.__name__, self.get_body())


class ChatServer:
    """This class implements interface for the server
    that has been received from loadbalancer."""

    def __init__(self, host, port):
        self.host = host
        self.port = int(port)
        self.reader = self.writer = None
        self.is_connected = False

    async def connect(self):
        if not self.is_connected:
            self.reader, self.writer = await asyncio.open_connection(self.host, self.port)
        self.is_connected = True

    def disconnect(self):
        self.writer.close()
        self.is_connected = False

    def write(self, data):
        server_logger.debug('Send raw data to server: %s', data)
        self.writer.write(data)

    async def read(self):
        header = await self.reader.read(HEADER_BUFFER_SIZE)
        packet_size = struct.unpack('=i', header)[0]
        if packet_size > 0:
            body = await self.reader.read(packet_size)
        data = header + body
        server_logger.debug('Received raw data from server: %s', data)
        return data

    def __del__(self):
        self.disconnect()

    def __str__(self):
        return '{0}: ({1}, {2})'.format(self.__class__.__name__, self.host, self.port)


class ChatClient:
    """The class implement simple interface for chat client."""

    def __init__(self, server, timeout=3):
        self.timeout = timeout
        self.server = server

    async def send(self, request):
        """
        The method sends request to chat server and waits for response
        :param request: request object
        :return: response object
        """
        await self.server.connect()
        server_logger.debug("Message has been sent with type code '%s' and body '%s'",
                      request.message_type_code, request.get_body())
        self.server.write(request.raw)
        response = ChatResponse(await self.server.read())
        server_logger.debug("Message has been received with type code '%s' and body '%s'",
                      response.message_type_code, response.body)

        # if request timeout has been received reconnect to server and send message again
        if response.message_type_code == 'ncdp':
            raise ChatConnectionTimeout(response.body)

        return response


class Chat:
    """High-level abstracted and simplified interface for communication with chat server of the glu."""

    def __init__(self, nickname='MetaServer0', player_id=1,  game_id='com.timeOfSilens.TimeOfSilens',
                 url='https://dev1.ghostwar.dairyfree.studio.glulive.com/edgeServer'):
        self.nickname = nickname
        self.player_id = str(player_id)
        self.url = url
        self.game_id = game_id
        self.client = ChatClient(server=self._get_server())

    def _get_server(self):
        response = requests.get(self.url)
        if response.status_code != 200:
            raise ValueError("Couldn't get servers from loadbalancer")
        response_json = response.json()
        return ChatServer(response_json['host'], response_json['port'])

    async def send_message(self, room_name, message):
        """
        Send specified message to specified room.
        WARNING: this method is asyncio coroutine so it should be used only in asyncio loop.
        :param room_name: name of target room (str)
        :param message: name of target room (str)
        :return: True if message was sent successfully or True if error was received
        """
        request = ChatBroadcastRoomMessageRequest(
            game_id=self.game_id,
            player_id=self.player_id,
            room_names=[room_name, ],  # rewrite to room_names if it will be needed
            message=message
        )
        response = await self.client.send(request)
        if not response.is_error():
            return True
        return False

    async def join_to_room(self, room_name, room_type='REGULAR_ROOM'):
        """
        Join to specified room.
        WARNING: this method is asyncio coroutine so it should be used only in asyncio loop.
        :param room_name: name of target room (str)
        :param room_type: type of target room (str). it can be only 'REGULAR_ROOM' or 'MANAGED_ROOM'
        :return: True if joining to room was performed successfully or True if error was received
        """
        request = ChatAddMemberToRoomRequest(
            game_id=self.game_id,
            player_id=self.player_id,
            room_name=room_name,
            room_type=room_type
        )
        response = await self.client.send(request)
        if not response.is_error():
            return True
        return False

    async def login(self):
        """
        Log in to the chat server.
        WARNING: this method is asyncio coroutine so it should be used only in asyncio loop.
        :return: True if login was performed successfully or True if error was received
        """
        request = ChatLoginPlayerRequest(
            game_id=self.game_id,
            nickname=self.nickname,
            player_id=self.player_id
        )
        response = await self.client.send(request)
        if not response.is_error():
            return True
        return False